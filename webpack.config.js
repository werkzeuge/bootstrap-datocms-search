const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
	entry: ['./src/index.js'],
	output: {
	  	path: path.resolve(__dirname, 'dist'),
	  	filename: 'bootstrap-datocms-search.js',
      sourceMapFilename: 'bootstrap-datocms-search.js.map',
	  	library: 'BDS',
	},
  module: {
      rules: [
          {
              test: /\.js$/,
              exclude: /node_modules/,
              use: {
                  loader: 'babel-loader',
                  options: {
                      presets: ['babel-preset-env'],
                      plugins: ['babel-plugin-transform-runtime'],
                  }
              }
          }
      ]
  },
  devtool: 'source-map',
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        extractComments: true,
        sourceMap: true,
      })
    ]
  }
};
