import App from './app';

const app = new App();

document.addEventListener('DOMContentLoaded', function() {
  app.handle();
});
