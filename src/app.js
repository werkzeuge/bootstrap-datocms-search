import DatoCmsSearch from 'datocms-search/dist/datocms-search.widget';

export default class App {
  handle() {
    this._load();
  }

  _load() {
    const searchSelector = 'link[rel="datocms-search"]';
    const search = this._querySelector(searchSelector);
    if (search) {
      this.environment = search.getAttribute('type');
      this.token = search.getAttribute('href');
      this.search = new DatoCmsSearch(this.token, this.environment);
      console.log(`Loaded ${this.environment} search with ${this.token}`);
      this.search.addWidget('#search', {});
    }
  }

  _querySelector(selector) {
    return document.querySelector(selector);
  }
}
